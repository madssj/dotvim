Installation
============
You probably want to fork my repository so you can make it your own.

```
git clone https://github.com/madssj/dotvim ~/.vim
ln -s ~/.vim/dotvimrc ~/.vimrc

cd ~/.vim/
git submodule init
git submodule update
```

Adding a module
===============
To add another vim module, simply add it as a git submodule (where applicable
of course) or as a regular file.

To add a submodule into the bundle folder, simply:

```
cd ~/.vim
git submodule add <remote> bundle/<module-name>
```

For example, to add vim-fugitive:

```
cd ~/.vim
git submodule add git://github.com/tpope/vim-fugitive.git bundle/fugitive
```

Updating submodules
===================
```git submodule foreach git pull```
